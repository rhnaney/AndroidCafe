﻿Public Class Form1
    Dim poweroff As Integer

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Label6.Text = 600
        Shell("taskkill /f /im explorer.exe")

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        Me.TopMost = True
        Form2.Timer1.Stop()
        Form2.Close()
        Timer2.Start()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            WebBrowser1.Document.GetElementById("user").SetAttribute("value", TextBox1.Text)
            WebBrowser1.Document.GetElementById("pass").SetAttribute("value", TextBox2.Text)

            WebBrowser1.Document.GetElementById("login").InvokeMember("click")

            Timer1.Start()
        Catch ex As Exception
            MessageBox.Show("Error, Please try again")
        End Try
       

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            Dim response As String = WebBrowser1.Document.GetElementById("status2").GetAttribute("value")

            If (response.Equals("loggedin")) Then
                Form2.Show()
            ElseIf (response.Equals("loggedout")) Then
                MessageBox.Show("Blee")
            End If
        Catch ex As Exception

        End Try
        
    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        poweroff = Label6.Text
        poweroff = poweroff - 1

        If (poweroff < 1) Then
            Shell("Shutdown /s")
        Else
            Label6.Text = poweroff
        End If

        Threading.Thread.Sleep(1000)
    End Sub
End Class
