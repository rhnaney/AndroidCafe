﻿Public Class Form2

    Dim x As Integer = 10
    Dim y As Integer
    Dim z As Integer
    Dim w As Integer

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        System.Diagnostics.Process.Start("explorer.exe")

        Form1.Hide()
        Form1.Timer1.Stop()
        Form1.Timer2.Stop()

        WebBrowser1.Navigate("http://192.168.1.160:8080/clientprofile.php")

        Timer1.Start()



    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Try
            Dim response As Integer = WebBrowser1.Document.GetElementById("voucher").GetAttribute("value")
            Dim responsebal As Integer = WebBrowser1.Document.GetElementById("balance").GetAttribute("value")

            If ((response > 0) Or (responsebal > 0)) Then
                x = response * 60
                y = responsebal
                w = response
                Timer2.Start()
            Else
                Form1.TextBox1.Clear()
                Form1.TextBox2.Clear()
                Form1.Label6.Text = 600
                Form1.Timer2.Start()
                Shell("taskkill /f /im explorer.exe")
                Form1.Show()
                Me.Close()
            End If

        Catch ex As Exception
        End Try

    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick
        Timer1.Stop()
       
        If (x > 0) Then
            x = x - 1
            Label1.Text = x
            Label2.Text = y
        Else
            Timer2.Stop()
            WebBrowser1.Refresh()
            Timer1.Start()
        End If

        Try
            z = (((x / 60) * 2) - (0.5)) - (x / 60)
            WebBrowser1.Document.GetElementById("returnvouch").SetAttribute("value", z)
            WebBrowser1.Document.GetElementById("returnbal").SetAttribute("value", w)

        Catch ex As Exception

        End Try

        Threading.Thread.Sleep(1000)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Timer2.Stop()
        Try
            WebBrowser1.Document.GetElementById("logout").InvokeMember("click")

            Form1.TextBox1.Clear()
            Form1.TextBox2.Clear()
            Form1.Label6.Text = 600
            Form1.Timer2.Start()
            Shell("taskkill /f /im explorer.exe")
            Form1.Show()
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("Error, Please try again")
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Shell("Shutdown /s")
    End Sub
End Class