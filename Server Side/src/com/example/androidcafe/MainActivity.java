package com.example.androidcafe;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class MainActivity extends Activity {
	
	@SuppressLint("NewApi")
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Button login = (Button) findViewById(R.id.login);
		Button signup = (Button) findViewById(R.id.signup);
		
		login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String is = null;
				
				EditText text1 = (EditText) findViewById(R.id.user);
				EditText text2 = (EditText) findViewById(R.id.pass);
				
				String v1 = text1.getText().toString();
				String v2 = text2.getText().toString();
				
							
				ArrayList<NameValuePair>nameValuePairs = new ArrayList<NameValuePair>();
				
				nameValuePairs.add(new BasicNameValuePair("f1", v1));
				nameValuePairs.add(new BasicNameValuePair("f2", v2));
				
				StrictMode.setThreadPolicy(policy);
				
				//http post
				try
				{
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost("http://192.168.1.160:8080/login.php");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					is= EntityUtils.toString(entity);
					
					try
					{
						JSONObject json_data = new JSONObject(is);
						CharSequence w = (CharSequence) json_data.get("re");
						
						if(w.equals("ok"))
						{
							sendUser(v1);
							Intent i=new Intent(getBaseContext(), profile.class);
							startActivity(i);
							finish();

						}
						else
						{
							Toast.makeText(getApplicationContext(), "No Matched!", Toast.LENGTH_SHORT).show();
						}
					}catch(JSONException e)
					{
						Log.e("log_tag", "Error parsing data " +e.toString());
						Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
					}
				}catch(Exception e)
				{
					Log.e("log_tag", "Error in http connection "+e.toString());
					Toast.makeText(getApplicationContext(), "No Connection!", Toast.LENGTH_SHORT).show();
				}
				
				//convert response to string
			}

			private void sendUser(final String v1) {
				profile.Query(v1);
				return;			
			}
		});
		
		signup.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i=new Intent(getBaseContext(), register.class);
				startActivity(i);
				finish();
				
			}
		});
	}

}
