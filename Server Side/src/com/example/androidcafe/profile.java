package com.example.androidcafe;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class profile extends Activity {
	
	@SuppressLint("NewApi")
	StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	
	private static String Q="";
	public static void Query(String q) 
	{
		Q=q;
	}
	TextView userlog, balancelog, pesolog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.profile);
	
	userlog = (TextView) findViewById(R.id.userlog);
	balancelog = (TextView) findViewById(R.id.balancelog);
	pesolog = (TextView) findViewById(R.id.pesolog);
	Button logout = (Button) findViewById(R.id.logout);
	Button coins = (Button) findViewById(R.id.coin);
	
	sendinfo();
	
	logout.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
				Intent i=new Intent(getBaseContext(), MainActivity.class);
				startActivity(i);
				finish();
			
			}
		});
	
	coins.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
				sendUser(Q);
				Intent i=new Intent(getBaseContext(), coin.class);
				startActivity(i);
				finish();
				
			}

		private void sendUser(final String Q) {
			coin.Query(Q);
			return;		
			
		}
		});
	}

	private void sendinfo() {
		
		String is = null;
		
		String v1 = Q;
		
					
		ArrayList<NameValuePair>nameValuePairs = new ArrayList<NameValuePair>();
		
		nameValuePairs.add(new BasicNameValuePair("f1", v1));
		
		StrictMode.setThreadPolicy(policy);
		
		//http post
		try
		{
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://192.168.1.160:8080/check.php");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is= EntityUtils.toString(entity);
			
			try
			{
				JSONObject json_data = new JSONObject(is);
				CharSequence w1 = (CharSequence) json_data.get("re");
				
				if(!w1.equals("ok"))
				{
					Toast.makeText(getApplicationContext(), "ok", Toast.LENGTH_SHORT).show();
					sendinformation(Q, w1);
				}
			}catch(JSONException e)
			{
				Log.e("log_tag", "Error parsing data " +e.toString());
				Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_SHORT).show();
			}
		}catch(Exception e)
		{
			Log.e("log_tag", "Error in http connection "+e.toString());
			Toast.makeText(getApplicationContext(), "No Connection!", Toast.LENGTH_SHORT).show();
		}
		
		return;
	}

	private void sendinformation(final String user, final CharSequence w1) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				userlog.setText(user);
				balancelog.setText(w1);
			}
		});
		return;
	}



}
