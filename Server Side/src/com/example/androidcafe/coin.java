package com.example.androidcafe;

import ioio.lib.api.AnalogInput;
import ioio.lib.api.exception.ConnectionLostException;
import ioio.lib.util.BaseIOIOLooper;
import ioio.lib.util.IOIOLooper;
import ioio.lib.util.android.IOIOActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class coin extends IOIOActivity {
	
	private static String Q="";
	public static void Query(String q) 
	{
		Q=q;
	}
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.coin);
		
		Button button1 = (Button) findViewById(R.id.button1);
		
		button1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				sendUser(Q);
				Intent i=new Intent(getBaseContext(), profile.class);
				startActivity(i);
				finish();
				
			}

			private void sendUser(final String q) {
				profile.Query(Q);
				return;	
				
			}
		});
	}
	
	class Looper extends BaseIOIOLooper {
		private AnalogInput uart;
		private AnalogInput uart1;
		private AnalogInput uart2;
		
		String x, z1, z2, z3;
		String y;
		String z;
		
		int y1=0;
		int y2=0;
		int y3=0;

		
		@Override
		protected void setup() throws ConnectionLostException {
			uart = ioio_.openAnalogInput(33);
			uart1 = ioio_.openAnalogInput(36);
			uart2 = ioio_.openAnalogInput(39);
		}

		
		@Override
		public void loop() throws ConnectionLostException, InterruptedException {
			
			float x1 = uart.read() * 10;
			float x2 = uart1.read() * 10;
			float x3 = uart2.read() * 10;
			
			
			x = Float.toString(x1);
			y = Float.toString(x2);
			z = Float.toString(x3);
			
			if((x=="0.0")&&(y=="0.0")&&(z=="0.0"))
			{
				x="6";
				y="6";
				z="6";
			}
			
			if(x=="0.0")
			{
				sendUser1(Q);
				Intent i=new Intent(getBaseContext(), coin1.class);
				startActivity(i);
				Thread.sleep(500);	
			}
			
			if(y=="0.0")
			{
				sendUser2(Q);
				Intent i=new Intent(getBaseContext(), coin5.class);
				startActivity(i);
				Thread.sleep(500);
			}
			
			if(z=="0.0")
			{
				sendUser3(Q);
				Intent i=new Intent(getBaseContext(), coin10.class);
				startActivity(i);
				Thread.sleep(500);	
			}		
		}


		private void sendUser1(final String q) {
			coin1.Query(q);
			return;	
			
		}


		private void sendUser2(final String q) {
			coin5.Query(q);
			return;	
			
		}


		private void sendUser3(final String q) {
			coin10.Query(q);
			return;	
			
		}
	}

	
	@Override
	protected IOIOLooper createIOIOLooper() {
		return new Looper();
	}

}
